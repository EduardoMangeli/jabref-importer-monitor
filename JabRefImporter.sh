#!/bin/bash

# configuration
EXECUTABLE=~/JabRef/bin/JabRef
WATCHED=watched
LOCK=lock.lck

flock -xn "$LOCK" -c "./do_JabRefImport.sh ${EXECUTABLE} ${WATCHED}"
