# JabRef Importer Monitor

A folder monitor to import .bib files into JabRef Database

## Motivation

Import BibTex citations made by Google Scholar or Zotero into JabRef open Database.

Based on:
- https://tex.stackexchange.com/questions/79251/improving-a-workflow-for-importing-bibtex-citations
- https://unix.stackexchange.com/questions/24952/script-to-monitor-folder-for-new-files


## Dependency

sudo apt install inotify-tools

## JabRef configuration

Go to Options->Preferences->Advanced and mark `Listen for remote operation on port XXXX`. The port number doesn't really matter.

## Install

Just clone this repository.

## Configuration

1. Configure `$EXECUTABLE` and `$WATCHED` variables in the file `JabRefImporter.sh`

| variable | content |
|---       |---      |
|`$EXECUTABLE`| JabRef file full path |
|`$WATCHED`| watched folder path |

2. make files executable
```bash
chmod +x JabRefImporter.sh do_JabRefImport.sh stop.sh
```

## Using
1. execute JabRef
1. with JabRef running, execute JabRefImporter.sh
1. save or move *.bib* files into `$WATCHED`
1. finish importing in JabRef shown dialog

- The monitor will continue working until **stop.sh** execution. So, saving or
moving *.bib* files in `$WATCHED` will start the importing process.

## Video
![Running](jabref-monitor.mp4)
