#!/bin/bash

inotifywait --monitor --event create --event moved_to "$2"|
    while read "$2" action file; do
        full_path=`realpath ${2}`
        echo "${full_path}/${file}\n"
        #java -jar "$1" --importToOpen "${full_path}/${file}"
        "$1" --importToOpen "${full_path}/${file}"
        rm "${full_path}/${file}"
    done
